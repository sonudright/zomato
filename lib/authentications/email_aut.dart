import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../home.dart';


//import '../home_page.dart';



class LoginAuth extends StatefulWidget {
  const LoginAuth({Key? key}) : super(key: key);

  @override
  State<LoginAuth> createState() => _Page1State();
}

class _Page1State extends State<LoginAuth> {


  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  validate(){
    if(!emailController.text.contains("@gmail.com")){
      Fluttertoast.showToast(msg: "Invalid Email");
    }
    else if(passwordController.text.length <=4){
      Fluttertoast.showToast(msg: "Enter min  characters");
    }
    else {
      emailAuth();
    }
  }

  emailAuth(){
    FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: emailController.text,
        password: passwordController.text
    ).then((value) => {
      userData()
    });
  }

  userData(){
    var auth=FirebaseAuth.instance.currentUser?.uid;
    FirebaseFirestore.instance.collection("users").doc(auth).set({
      "email":emailController.text,
      "password":passwordController.text,
    }).then((value) => {
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => ZomatoHomePage(),))
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.red,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Center(child: Padding(
            padding: EdgeInsets.only(top: 40),
            child: Text("Email Sign In",style: TextStyle(color: Colors.blue,fontSize: 35,fontWeight: FontWeight.bold),),
          )),

          Padding(
            padding: const EdgeInsets.only(left: 30,right: 30,top: 20),
            child: TextField(
              controller: emailController,
              keyboardType:TextInputType.text,
              decoration: InputDecoration(
                labelText: "Enter Email",
                border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),

              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30,right: 30,top: 20),
            child: TextField(
              controller: passwordController,
              keyboardType:TextInputType.text,
              decoration: InputDecoration(

                labelText: "Enter Password",
                border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),

              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: ElevatedButton(
                onPressed: (){
                  validate();
                },
                child: const Text("Submit")
            ),
          )
        ],
      ),
    );

  }
}
