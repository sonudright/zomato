// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:flutter/material.dart';
// import 'package:fluttertoast/fluttertoast.dart';
// import 'package:zomato/verify_otp.dart';
//
// import 'home.dart';
//
// class PhoneAuth extends StatefulWidget {
//   const PhoneAuth({Key? key}) : super(key: key);
//
//   @override
//   State<PhoneAuth> createState() => _PhoneAuthState();
// }
//
// class _PhoneAuthState extends State<PhoneAuth> {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       initialRoute: 'phone',
//       debugShowCheckedModeBanner: false,
//       routes: {
//         'phone': (context) => MyPhone(),
//         'verify': (context) => OtpPage()
//       },
//     );
//   }
// }
//
// class MyPhone extends StatefulWidget {
//   const MyPhone({Key? key}) : super(key: key);
//
//   static String verify = "";
//
//   @override
//   State<MyPhone> createState() => _MyPhoneState();
// }
//
// class _MyPhoneState extends State<MyPhone> {
//   TextEditingController countryController = TextEditingController();
//   var phone = "";
//
//   @override
//   void initState() {
//     countryController.text = "+91";
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.red,
//       body: Container(
//         margin: const EdgeInsets.only(left: 25, right: 25),
//         alignment: Alignment.center,
//         child: SingleChildScrollView(
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: [
//               Image.asset(
//                 'assests/images/img.png',
//                 width: 150,
//                 height: 150,
//               ),
//               const SizedBox(
//                 height: 25,
//               ),
//               const Text(
//                 "Phone Verification",
//                 style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
//               ),
//               const SizedBox(
//                 height: 10,
//               ),
//               const Text(
//                 "We need to register your phone without getting started!",
//                 style: TextStyle(
//                   fontSize: 16,
//                 ),
//                 textAlign: TextAlign.center,
//               ),
//               const SizedBox(
//                 height: 30,
//               ),
//               Container(
//                 height: 55,
//                 decoration: BoxDecoration(
//                     border: Border.all(width: 1, color: Colors.grey),
//                     borderRadius: BorderRadius.circular(10)),
//                 child: Row(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: [
//                     const SizedBox(
//                       width: 10,
//                     ),
//                     SizedBox(
//                       width: 40,
//                       child: TextField(
//                         controller: countryController,
//                         keyboardType: TextInputType.number,
//                         decoration: const InputDecoration(
//                           border: InputBorder.none,
//                         ),
//                       ),
//                     ),
//                     const Text(
//                       "|",
//                       style: TextStyle(fontSize: 33, color: Colors.grey),
//                     ),
//                     const SizedBox(
//                       width: 10,
//                     ),
//                     Expanded(
//                         child: TextField(
//                       onChanged: (value) {
//                         phone = value;
//                       },
//                       keyboardType: TextInputType.phone,
//                       decoration: const InputDecoration(
//                         border: InputBorder.none,
//                         hintText: "Phone",
//                       ),
//                     ))
//                   ],
//                 ),
//               ),
//               const SizedBox(
//                 height: 20,
//               ),
//               SizedBox(
//                 width: double.infinity,
//                 height: 45,
//                 child: ElevatedButton(
//                     style: ElevatedButton.styleFrom(
//                         primary: Color(0xFF2661FA),
//                         shape: RoundedRectangleBorder(
//                             borderRadius: BorderRadius.circular(10))),
//                     onPressed: () async {
//                       try {
//                         await FirebaseAuth.instance.verifyPhoneNumber(
//                           phoneNumber: '${countryController.text + phone}',
//                           timeout: const Duration(seconds: 5),
//                           verificationCompleted:
//                               (PhoneAuthCredential credential) {},
//                           verificationFailed: (FirebaseAuthException e) {},
//                           codeSent: (String verificationId, int? resendToken) {
//                             MyPhone.verify = verificationId;
//                             Navigator.pushNamed(context, 'verify');
//                           },
//                           codeAutoRetrievalTimeout: (String verificationId) {},
//                         );
//                         Fluttertoast.showToast(msg: "OTP sent");
//                       } catch (e) {
//                         Fluttertoast.showToast(msg: "Error");
//                       }
//
//                       Navigator.pushNamed(context, 'verify');
//                     },
//                     child: const Text("Send the code")),
//               )
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
//
//
//

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:zomato/verify_otp.dart';

import 'authentications/email_aut.dart';
//import 'package:zomato/authantication/verify_otp.dart';


//import 'email_auth.dart';




class PhoneNumber extends StatefulWidget {
  const PhoneNumber({Key? key}) : super(key: key);

  static String verify = "";

  @override
  State<PhoneNumber> createState() => _PhoneNumberState();
}

class _PhoneNumberState extends State<PhoneNumber> {

  TextEditingController phoneController=TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.red,
      body: ListView(
        children: [
          SizedBox(
              width: 600,
              height: 280,
              child: Image.asset("assets/images/logo.jpg",width: 100,height: 150,)),

          IntlPhoneField(
            controller: phoneController,
            flagsButtonPadding: const EdgeInsets.all(8),
            dropdownIconPosition: IconPosition.trailing,
            decoration: const InputDecoration(
              labelText: 'Phone Number',
              border: OutlineInputBorder(
                borderSide: BorderSide(),
              ),
            ),
            initialCountryCode: 'IN',
            onChanged: (phone) {
              print(phone.completeNumber);
            },
          ),

          ElevatedButton(
              onPressed: () async {
                try {
                  await FirebaseAuth.instance.verifyPhoneNumber(
                    phoneNumber: '+91${phoneController.text}',
                    verificationCompleted: (PhoneAuthCredential credential) {},
                    verificationFailed: (FirebaseAuthException e) {},
                    codeSent: (String verificationId, int? resendToken) {
                      PhoneNumber.verify=verificationId;
                      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => const OtpPage(),));
                      Fluttertoast.showToast(msg: "Sent OTP");
                    },
                    codeAutoRetrievalTimeout: (String verificationId) {},
                  );
                }
                catch(e){
                  Fluttertoast.showToast(msg: "OTP Failed");
                }
              },
              child: const Text("Send Otp")
          ),
          Center(
            child: Row(
              children:  [

                Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: GestureDetector(
                    onTap: () {
                      signInWithGoogle();
                    },
                    child: const Card(
                      child: CircleAvatar(
                        backgroundImage: AssetImage("assets/images/google.png"),
                      ),
                    ),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LoginAuth() )) ;
                    },
                    child: const Card(
                      child: CircleAvatar(
                        backgroundImage: AssetImage("assets/images/email.jpg"),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<User?> signInWithGoogle() async {
    final FirebaseAuth _auth = FirebaseAuth.instance;
    final GoogleSignIn googleSignIn = GoogleSignIn();
    try {
      final GoogleSignInAccount? googleSignInAccount = await googleSignIn.signIn();
      if (googleSignInAccount != null) {
        final GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;
        final AuthCredential credential = GoogleAuthProvider.credential(
          accessToken: googleSignInAuthentication.accessToken,
          idToken: googleSignInAuthentication.idToken,
        );
        final UserCredential authResult = await _auth.signInWithCredential(credential);
        return authResult.user;
      }
    } catch (error) {
      print("Google Sign-In Error: $error");
      return null;
    }
  }
}
  //
  // Future<User?> signInWithGoogle() async {
  //   final FirebaseAuth _auth = FirebaseAuth.instance;
  //   final GoogleSignIn googleSignIn = GoogleSignIn();
  //   try {
  //     final GoogleSignInAccount? googleSignInAccount = await googleSignIn.signIn();
  //     if (googleSignInAccount != null) {
  //       final GoogleSignInAuthentication googleSignInAuthentication =
  //       await googleSignInAccount.authentication;
  //       final AuthCredential credential = GoogleAuthProvider.credential(
  //         accessToken: googleSignInAuthentication.accessToken,
  //         idToken: googleSignInAuthentication.idToken,
  //       );
  //       final UserCredential authResult = await _auth.signInWithCredential(credential);
  //       return authResult.user;
  //     }
  //   } catch (error) {
  //     print("Google Sign-In Error: $error");
  //     return null;
  //
  //   }
  //
  // }

