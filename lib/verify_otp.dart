import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pinput/pinput.dart';

import 'home.dart';
import 'login.dart';

//import '../home_page.dart';
//import 'loginphone_auth.dart';

class OtpPage extends StatefulWidget {
  const OtpPage({Key? key}) : super(key: key);

  @override
  State<OtpPage> createState() => _OtpPageState();
}

class _OtpPageState extends State<OtpPage> {
  TextEditingController pinController = TextEditingController();

  FirebaseAuth auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.red,
        body: Padding(
          padding: const EdgeInsets.all(50),
          child: Column(
            children: [
              SizedBox(
                  width: 600,
                  height: 280,
                  child: Image.asset("assets/image/burger.jpg",width: 100,height: 150,)),
              Pinput(
                keyboardType: TextInputType.number,
                controller: pinController,
                length: 6,
                showCursor: true,
                pinputAutovalidateMode: PinputAutovalidateMode.onSubmit,
                textInputAction: TextInputAction.next,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30),
                child: ElevatedButton(
                    onPressed: () async {
                      try {
                        PhoneAuthCredential credential = PhoneAuthProvider.credential(
                            verificationId: PhoneNumber.verify,
                            smsCode: pinController.text
                        );
                        // Sign the user in (or link) with the credential
                        await auth.signInWithCredential(credential);
                        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) =>  ZomatoHomePage()));
                        Fluttertoast.showToast(msg: "Login successful");
                      } catch (e) {
                        Fluttertoast.showToast(msg: "Enter Valid OTP");
                      }
                    },
                    child: const Text("Enter Otp")),
              ),
            ],
          ),
        ),
      ),
    );
  }
}