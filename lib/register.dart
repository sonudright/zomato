//
//
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter/material.dart';
// import 'package:fluttertoast/fluttertoast.dart';
//
// import 'home.dart';
//
//
//
// class registerpage extends StatefulWidget {
//   registerpage({Key? key}) : super(key: key);
//
//   @override
//   State<registerpage> createState() => _registerpageState();
// }
//
// class _registerpageState extends State<registerpage> {
//   TextEditingController nameController = TextEditingController();
//   TextEditingController emailController = TextEditingController();
//   TextEditingController phoneController = TextEditingController();
//   TextEditingController passwordController = TextEditingController();
//
//   Validata(){
//     if(nameController.text.isEmpty){
//       Fluttertoast.showToast(msg: "Please fill name");
//     }
//     else if(nameController.text.length <=3){
//       Fluttertoast.showToast(msg: "Invalid Name");
//     }
//     else if( !nameController.text.startsWith(RegExp(r'[A-Z]'))){
//       Fluttertoast.showToast(msg: "Name Must Start With Capital letter");
//     }
//     else if(nameController.text.contains(RegExp(r'[0-9]'))){
//       Fluttertoast.showToast(msg:"Number Not allowed Into Name");
//     }
//     else if(!nameController.text.contains(RegExp(r'[a-z]'))){
//       Fluttertoast.showToast(msg:"Name Should mix of uppercase and  lowercase");
//     }
//     else if(emailController.text.isEmpty){
//       Fluttertoast.showToast(msg: "please enter email");
//     }
//     else if (!emailController.text.contains("@gmail.com")){
//       Fluttertoast.showToast(msg: "Invalid Email");
//     }
//     else if (!phoneController.text.contains(RegExp(r'[0-9]')) ){
//       Fluttertoast.showToast(msg: "phone number is must be number");
//     }
//     else if(phoneController.text.length <10){
//       Fluttertoast.showToast(msg: "plese inter number 10 digite");
//     }
//     else if (phoneController.text.isEmpty){
//       Fluttertoast.showToast(msg: "please enter number");
//     }
//     else if(passwordController.text.isEmpty){
//       Fluttertoast.showToast(msg: "please enter password");
//     }
//     else if(passwordController.text.length <= 8){
//       Fluttertoast.showToast(msg: "password must be 8 character");
//     }
//     else if(!passwordController.text.startsWith(RegExp(r'[A-Z]'))){
//       Fluttertoast.showToast(msg: "password Must Start With Capital letter");
//     }
//     else if(!passwordController.text.contains(RegExp(r'[a-z]'))){
//       Fluttertoast.showToast(msg: "plese enter lowercase  password");
//     }
//     else if (!passwordController.text.contains(RegExp(r'[0-9]')) ){
//       Fluttertoast.showToast(msg: "plese enter number psssword");
//     }
//     else if (!passwordController.text.contains(RegExp(r'[$@#^&{:,._+!>?]')) ){
//       Fluttertoast.showToast(msg: "plese enter spacial caractor password ");
//     }
//     else{
//       userData();
//     }
//   }
//
//   userData(){
//     var auth="1";
//     FirebaseFirestore.instance.collection("users").doc(auth).set({
//       "name":nameController.text,
//       "email":emailController.text,
//       "phone":phoneController.text,
//       "password":passwordController.text,
//     }).then((value) => {
//
//       Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => BottomNavigation (),))
//     }).catchError((error, stackTrace) => {
//       Fluttertoast.showToast(msg: "" + error.toString())
//     });
//   }
//   @override
//   Widget build(BuildContext context) {
//     return  Scaffold(
//       backgroundColor: Colors.red,
//       body: ListView(
//         children: [
//           const Center(child: Padding(
//             padding: EdgeInsets.only(top: 40),
//             child: Text("Register",style: TextStyle(color: Colors.blue,fontSize: 35,fontWeight: FontWeight.bold),),
//           )),
//           Padding(
//             padding: const EdgeInsets.only(left: 30,right: 30,top: 10),
//             child: TextField(
//               controller: nameController,
//               decoration: InputDecoration(
//
//                 label:const Text("Enter Name"),
//                 border: OutlineInputBorder(
//
//                   borderRadius: BorderRadius.circular(10),
//
//                 )
//             ),),
//           ),
//           Padding(
//             padding: const EdgeInsets.only(left: 30,right: 30,top: 10),
//             child: TextField(
//               controller: emailController,
//               decoration: InputDecoration(
//                 label:Text("Enter Email"),
//                 border: OutlineInputBorder(
//                   borderRadius: BorderRadius.circular(10),
//
//                 )
//             ),),
//           ),
//           Padding(
//             padding: const EdgeInsets.only(left: 30,right: 30,top: 10),
//             child: TextField(
//               keyboardType: TextInputType.number,
//               maxLength: 10,
//               controller: phoneController,
//               decoration: InputDecoration(
//                 label:Text("Enter Phone"),
//                 border: OutlineInputBorder(
//                   borderRadius: BorderRadius.circular(10),
//
//                 )
//             ),),
//           ),
//           Padding(
//             padding: const EdgeInsets.only(left: 30,right: 30,top: 10),
//             child: TextField(
//
//               controller: passwordController,
//               decoration: InputDecoration(
//                 label:Text("Enter Password"),
//                 border: OutlineInputBorder(
//                   borderRadius: BorderRadius.circular(10),
//
//                 )
//             ),),
//           ),
//           Padding(
//             padding: const EdgeInsets.only(left: 30,right: 30,top: 10),
//             child: ElevatedButton(onPressed: () {
//               Validata();
// Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => BottomNavigationBarExample(),));
//             }, child: Text("register")),
//           )
//
//         ],
//       ),
//     );
//   }
// }